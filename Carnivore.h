//
// Created by Sandra on 28.05.2020.
//

#ifndef ZOO_PROJECT_CARNIVORE_H
#define ZOO_PROJECT_CARNIVORE_H


#include "Animal.h"

class Carnivore: public Animal {
public:
    void eat(Field field);

    Carnivore(int maxThirst, int maxHunger, int size, const std::string &species);
};


#endif //ZOO_PROJECT_CARNIVORE_H
