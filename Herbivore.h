//
// Created by Sandra on 28.05.2020.
//

#ifndef ZOO_PROJECT_HERBIVORE_H
#define ZOO_PROJECT_HERBIVORE_H


#include "Animal.h"

class Herbivore: public Animal {
public:
    void eat(Field field);

    Herbivore(int maxThirst, int maxHunger, int size, const std::string &species);
};


#endif //ZOO_PROJECT_HERBIVORE_H
