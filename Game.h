//
// Created by Sandra on 14.05.2020.
//

#ifndef ZOO_PROJECT_GAME_H
#define ZOO_PROJECT_GAME_H


#include <vector>
#include "Animal.h"
#include "Map.h"

class Game {
private:
    Map map;
    int counter = 0;
    void inputCoords();
    void inputAnimals();
    void inputFields();
    void satisfy();
    void moveAnimals();
    void saveAnimals(int kind_of_animal, std::string species, int size_of_animal, int max_hunger, int max_thirst);
    void ChooseUrKindOfAnimal();
    void ChooseAnimalSize(int kind_of_animal, std::string species);
    void setMaxHunger(int kind_of_animal, std::string species, int size_of_animal);
    void setMaxThirst(int kind_of_animal, std::string species, int size_of_animal, int max_hunger);
    void HerbivoreSpecies(int kind_of_animal);
    void CarnivoreSpecies(int kind_of_animal);

public:
    Game();

    void play();

    const Map &getMap() const;

    void setMap(const Map &map);



};


#endif //ZOO_PROJECT_GAME_H
