//
// Created by Sandra on 14.05.2020.
//

#include "Map.h"
#include <iostream>
#include <time.h>

using namespace std;

Map::Map(int column, int row) : column(column), row(row) {
    vector<vector<Field*>> vector;
    for (int i = 0; i < column; i++){
        std::vector<Field*> wtf;
        for (int j =0; j < row; j++){
            wtf.push_back(new Field());
        }
        vector.push_back(wtf);
    }
    plan = vector;
}

Map::Map() {}

int Map::getColumn() const {
    return column;
}

int Map::getRow() const {
    return row;
}

Field* Map::getField(int x, int y) {
    return plan.at(y).at(x);
}

void Map::makeWater(int water) {
    while (water > 0){
        srand(clock());
        int random_row = rand() % row;
        int random_col = rand() % column;
        if (!getField(random_row,random_col)->isWater()){
            water--;
            getField(random_row,random_col)->setWater(true);
        }
    }
}

void Map::makeGrass(int grass) {
    while (grass > 0){
        srand(clock());
        int random_row = rand() % row;
        int random_col = rand() % column;
        if (!getField(random_row,random_col)->isWater() && !getField(random_row,random_col)->isGrass()){
            grass--;
            getField(random_row,random_col)->setGrass(true);
        }
    }

}
