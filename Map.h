//
// Created by Sandra on 14.05.2020.
//

#ifndef ZOO_PROJECT_MAP_H
#define ZOO_PROJECT_MAP_H


#include <vector>
#include "Field.h"

class Map {
private:
    int column;
    int row;
    std::vector<std::vector<Field*>> plan;
public:
    Map();
    void makeWater(int water);
    void makeGrass(int grass);
    Map(int column, int row);
    Field* getField(int x,int y);

    int getColumn() const;

    int getRow() const;


};


#endif //ZOO_PROJECT_MAP_H
