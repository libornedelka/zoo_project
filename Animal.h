//
// Created by Libor on 11.5.2020.
//

#ifndef ZOO_PROJECT_ANIMAL_H
#define ZOO_PROJECT_ANIMAL_H
#include <iostream>
#include "Field.h"

class Animal {
    int max_thirst;
    int max_hunger;
    int hunger;
    int thirst;
    int size;
    int row;
    int column;
    std::string species;
    void moveUp(int row, int col);
    void moveDown(int row, int col);
    void moveRight(int row, int col);
    void moveLeft(int row, int col);
public:
    Animal();

    Animal(int maxThirst, int maxHunger, int size, const std::string &species);

    void setRow(int row);
    void setColumn(int column);
    void setHunger(int ate);
    void setThirst(int drank);
    int getThirst() const;
    int getHunger() const;
    int getSize() const;

    virtual void eat(Field field);
    void drink(Field field);

    std::string getSpecies() const;
    int getRow() const;
    int getColumn() const;

    bool operator==(const Animal &animal) const{
        return (hunger == animal.getHunger() && thirst == animal.getThirst() && size == animal.getSize()
                && row == animal.getRow() && column == animal.getColumn() && species == animal.getSpecies());
    }
    bool operator!=(const Animal &animal) const{
        return !(hunger == animal.getHunger() && thirst == animal.getThirst() && size == animal.getSize()
                && row == animal.getRow() && column == animal.getColumn() && species == animal.getSpecies());
    }
    void move(int row, int col);
    void fillHunger();
    void fillThirst();
    bool hungerIsSated();
};


#endif //ZOO_PROJECT_ANIMAL_H
