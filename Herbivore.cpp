//
// Created by Sandra on 28.05.2020.
//

#include "Herbivore.h"

void Herbivore::eat(Field field) {
    if (field.isGrass()) {
        fillHunger();
        std::cout << getSpecies() << " just ate." << std::endl;
    }
    else{
        setHunger(getHunger()-1);
    }
}

Herbivore::Herbivore(int maxThirst, int maxHunger, int size, const std::string &species) : Animal(maxThirst, maxHunger,
                                                                                                  size, species) {
    fillHunger();
    fillThirst();
}
