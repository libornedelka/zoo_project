//
// Created by Sandra on 14.05.2020.
//

#include "Game.h"
#include "AnimalManager.h"
#include <iostream>
using namespace std;

void Game::inputCoords() {
    int row;
    int column;
    cout << "Insert number of rows: " << endl;
    cin >> row;
    cout << "Insert number of columns: " << endl;
    cin >> column;
    const Map *map = new Map(column, row);
    cout << "Number of fields: " << row*column << endl;
    cout << "******************************************************" << endl;
    setMap(*map);
}

void Game::inputFields() {
    int water;
    int grass;
    cout << "Insert number of WATER fields: " << endl;
    cin >> water;
    cout << "Insert number of GRASS fields: " << endl;
    cin >> grass;
    map.makeWater(water);
    map.makeGrass(grass);
    cout << "******************************************************" << endl;
}

//odešle data AnimalManager
void Game::saveAnimals(int kind_of_animal, string species,  int size_of_animal, int max_hunger, int max_thirst){
    AnimalManager::addAnimal(kind_of_animal, species, size_of_animal, max_hunger, max_thirst, map.getRow(), map.getColumn());
}

//ve foru se pouze vola metoda ChooseurKindOfAnimal, kde si uživatel vybere druh zvířete, poté se to odešle
//na výběr velikosti a poté do AnimalManager s hodnotamy v proměnných
void Game::inputAnimals() {
    int number;

    cout << "Insert number of animals: " << endl;
    cin >> number;

    for (int i = 0; i < number; i++){
        ChooseUrKindOfAnimal();
    }
}
//kontrola vybraného druhu
void Game::ChooseUrKindOfAnimal(){
    int kind_of_animal;

    cout << "Choose what kind of animal do you want (1 / 2): \n1 : herbivore \n2 : carnivore" << endl;
    cin >> kind_of_animal;

    if (kind_of_animal == 1) {
        cout << "******************************************************" << endl;
        HerbivoreSpecies(kind_of_animal);
    }
    else if (kind_of_animal == 2){
        cout << "******************************************************" << endl;
        CarnivoreSpecies(kind_of_animal);
    }
    else{
        cout << "Wrong input, default animal type is herbivore. \nSetting up animal type to herbivore." << endl;
        cout << "******************************************************" << endl;
        HerbivoreSpecies(1);
    }
}
//vyber bylozravce
void Game::HerbivoreSpecies(int kind_of_animal){
    int animal_type;
    cout << "What kind of herbivore do you want to add? Choose with numbers." << endl;
    cout << "1 : deer\n2 : cow\n3 : goat\n4 : sheep\n5 : chicken" << endl;
    cin >> animal_type;

    switch (animal_type) {
        case 1:
            cout << "(You have chosen deer." << endl;
            ChooseAnimalSize(kind_of_animal, "Deer");
            return;
        case 2:
            cout << "(You have chosen cow." << endl;
            ChooseAnimalSize(kind_of_animal, "Cow");
            return;
        case 3:
            cout << "(You have chosen goat." << endl;
            ChooseAnimalSize(kind_of_animal, "Goat");
            return;
        case 4:
            cout << "(You have chosen sheep." << endl;
            ChooseAnimalSize(kind_of_animal, "Sheep");
            return;
        case 5:
            cout << "(You have chosen chicken." << endl;
            ChooseAnimalSize(kind_of_animal, "Chicken");
            return;
        default:
            cout << "(Wrong input, default herbivore animal is deer." << endl;
            ChooseAnimalSize(kind_of_animal, "Deer");
            return;
    }
}

//vyber masozravce
void Game::CarnivoreSpecies(int kind_of_animal){
    int animal_type;

    cout << "What kind of carnivore do you want to add? Choose with numbers." << endl;
    cout << "1 : wolf \n2 : fox \n3 : coyote \n4 : cat \n5 : jaguar" << endl;
    cin >> animal_type;

    switch (animal_type) {
        case 1:
            cout << "(You have chosen wolf.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Wolf");
            return;
        case 2:
            cout << "(You have chosen fox.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Fox");
            return;
        case 3:
            cout << "(You have chosen coyote.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Coyote");
            return;
        case 4:
            cout << "(You have chosen cat.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Cat");
            return;
        case 5:
            cout << "(You have chosen jaguar.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Jaguar");
            return;
        default:
            cout << "(Wrong input, default carnivore animal is wolf.)" << endl;
            ChooseAnimalSize(kind_of_animal, "Wolf");
            return;
    }
}
//nastavení max žízně a předání všech dat do saveAnimals
void Game::setMaxThirst(int kind_of_animal, string species, int size_of_animal, int max_hunger){
    int max_thirst;
    cout << "Choose max THIRST of your animal, MIN = 5, MAX = 200: " << endl;
    cin >> max_thirst;
    if(max_thirst < 5){
        cout << "(Setting up animal max thirst to: 5.)" << endl;
        cout << "******************************************************" << endl;
        saveAnimals(kind_of_animal, species, size_of_animal, max_hunger, 5);
    }
    else if (max_thirst > 200){
        cout << "(Setting up animal max thirst to: 200.)" << endl;
        cout << "******************************************************" << endl;
        saveAnimals(kind_of_animal, species, size_of_animal, max_hunger, 200);
    }
    else{
        cout << "(Setting up animal max thirst to: " << max_thirst << ".)" << endl;
        cout << "******************************************************" << endl;
        saveAnimals(kind_of_animal, species, size_of_animal, max_hunger, max_thirst);
    }
}

//nastavení max hladu
void Game::setMaxHunger(int kind_of_animal, string species, int size_of_animal){
    int max_hunger;
    cout << "Choose max HUNGER of your animal: MIN = 10, MAX = 200: " << endl;
    cin >> max_hunger;
    if(max_hunger < 10){
        cout << "(Setting up animal max hunger to: 10.)" << endl;
        cout << "******************************************************" << endl;
        setMaxThirst(kind_of_animal, species, size_of_animal, 10);
    }
    else if (max_hunger > 200){
        cout << "(Setting up animal max hunger to: 200.)" << endl;
        cout << "******************************************************" << endl;
        setMaxThirst(kind_of_animal, species, size_of_animal, 200);
    }
    else{
        cout << "(Setting up animal max hunger to: " << max_hunger << ".)" << endl;
        cout << "******************************************************" << endl;
        setMaxThirst(kind_of_animal, species,  size_of_animal, max_hunger);
    }
}

//kontrola vybrané velikosti
void Game::ChooseAnimalSize(int kind_of_animal, string species){
    int size_of_animal;
    cout << "******************************************************" << endl;
    cout << "Choose SIZE of your animal: MIN = 1, MAX = 500: " << endl;
    cin >> size_of_animal;
    if(size_of_animal <= 0){
        cout << "(Setting up MIN animal size to: 1.)" << endl;
        cout << "******************************************************" << endl;
        setMaxHunger(kind_of_animal, species, 1);
    }
    else if (size_of_animal > 500){
        cout << "(Setting up MAX animal size to: 500.)" << endl;
        cout << "******************************************************" << endl;
        setMaxHunger(kind_of_animal, species, 500);
    }
    else{
        cout << "(Setting up animal size to: " << size_of_animal << ".)" << endl;
        cout << "******************************************************" << endl;
        setMaxHunger(kind_of_animal, species, size_of_animal);
    }
}

void Game::play() {
    inputCoords();
    inputFields();
    inputAnimals();
    while (AnimalManager::getAnimals().size() > 0){
        counter++;
        moveAnimals();
        satisfy();
    }

}
void Game::satisfy() {
    for (Animal* animal: AnimalManager::getAnimals()) {
        animal->drink(*map.getField(animal->getRow(), animal->getColumn()));
        animal->eat(*map.getField(animal->getRow(), animal->getColumn()));
        //cout << "Species: " << animal->getSpecies() << "|" << " Hunger and thirst: " << animal->getHunger() << " " << animal->getThirst() << endl;
        if (animal->getThirst() <= 0 || animal->getHunger() <= 0){
            std::cout << "------------------------------------------------------"<< std::endl;
            cout << "*" << animal->getSpecies() << " died because of hunger or thirst." << endl;
            AnimalManager::removeAnimal(animal);
        }
    }
}
void Game::moveAnimals() {
    for (Animal* animal: AnimalManager::getAnimals()) {
        animal->move(map.getRow(), map.getColumn());
    }
}

const Map &Game::getMap() const {
    return map;
}

void Game::setMap(const Map &map) {
    Game::map = map;
}

Game::Game() {}
