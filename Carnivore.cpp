//
// Created by Sandra on 28.05.2020.
//

#include <vector>
#include "Carnivore.h"
#include "AnimalManager.h"

void Carnivore::eat(Field field) {
    bool ate = false;
    std::vector<Animal*>* animals = AnimalManager::getAnimalsOnPosition(getRow(),getColumn());
    if (animals->size() == 1) {
        setHunger(getHunger() - 1);
        return;
    }
    for (Animal* animal : *animals){
        if ((Animal*)this != animal && getSize() > animal->getSize() && !hungerIsSated()){
            ate = true;
            fillHunger();
            std::cout << "------------------------------------------------------"<< std::endl;
            std::cout << "*" << getSpecies() << " ate " << animal->getSpecies() << "." << std::endl;
            AnimalManager::removeAnimal(animal);
        }
    }

    if (!ate) {
        setHunger(getHunger() - 1);
        return;
    }
}

Carnivore::Carnivore(int maxThirst, int maxHunger, int size, const std::string &species) : Animal(maxThirst, maxHunger,
                                                                                                  size, species) {
    fillHunger();
    fillThirst();
}
