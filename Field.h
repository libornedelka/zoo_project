//
// Created by Sandra on 14.05.2020.
//

#ifndef ZOO_PROJECT_FIELD_H
#define ZOO_PROJECT_FIELD_H


class Field {
private:
    bool water = false;
    bool grass = false;
public:
    bool isWater() const;

    bool isGrass() const;

    void setWater(bool water);

    void setGrass(bool grass);
};


#endif //ZOO_PROJECT_FIELD_H
