//
// Created by Sandra on 25.05.2020.
//

#include <time.h>
#include "AnimalManager.h"
#include "Herbivore.h"
#include "Carnivore.h"

std::vector<Animal*> AnimalManager::animals;

const std::vector<Animal*> &AnimalManager::getAnimals(){
    return animals;
}

void AnimalManager::addAnimal(int kind_of_animal, std::string species, int size_of_animal, int max_hunger, int max_thirst, int row, int col) {
    srand(clock());
    int random_row = rand() % row;
    int random_col = rand() % col;
    if (kind_of_animal == 1) {
        Herbivore* herb = new Herbivore(max_thirst,max_hunger,size_of_animal,species);
        herb->setRow(random_row);
        herb->setColumn(random_col);
        animals.push_back(herb);
    }
    else{
        Carnivore* car = new Carnivore(max_thirst,max_hunger,size_of_animal,species);
        car->setRow(random_row);
        car->setColumn(random_col);
        animals.push_back(car);
    }
}

 std::vector<Animal*>* AnimalManager::getAnimalsOnPosition(int row, int column) {
    std::vector<Animal*>* result = new std::vector<Animal*>;
    for (Animal* a: animals) {
        if (a->getRow() == row && a->getColumn() == column){
            result->push_back(a);
        }
    }
    return result;
}

void AnimalManager::removeAnimal(Animal* animal) {
    for (int i=0; i < animals.size(); i++){
        if ( animals.at(i) == animal ) {
            std::cout << "There are currently " << getAnimals().size() - 1 << " animal(s)." << std::endl;
            std::cout << "------------------------------------------------------"<< std::endl;
            animals.erase(animals.begin()+i);
        }
    }
}