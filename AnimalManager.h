//
// Created by Sandra on 25.05.2020.
//

#ifndef ZOO_PROJECT_ANIMALMANAGER_H
#define ZOO_PROJECT_ANIMALMANAGER_H
#include <vector>
#include "Animal.h"
#include <algorithm>
#include "Game.h"

class AnimalManager {
    static std::vector<Animal*> animals;
public:
    static void addAnimal(int kind_of_animal, std::string species, int size_of_animal, int max_hunger, int max_thirst, int row, int col);
    static void removeAnimal(Animal* animal);

    static const std::vector<Animal*> &getAnimals();

    static std::vector<Animal*>* getAnimalsOnPosition(int row, int column);
};


#endif //ZOO_PROJECT_ANIMALMANAGER_H
