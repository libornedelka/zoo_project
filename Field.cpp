//
// Created by Sandra on 14.05.2020.
//

#include "Field.h"



bool Field::isWater() const {
    return water;
}

bool Field::isGrass() const {
    return grass;
}

void Field::setWater(bool water) {
    Field::water = water;
}

void Field::setGrass(bool grass) {
    Field::grass = grass;
}
