//
// Created by Libor on 11.5.2020.
//

#include "Animal.h"
#include <iostream>
#include <time.h>

Animal::Animal(){
}

int Animal::getThirst() const{
    return thirst;
}

int Animal::getHunger() const{
    return hunger;
}

void Animal::move(int row, int col) {
    srand(clock()); //zpusobuje random cisla
    int random_num = rand() % 4;

    switch (random_num) {
        case 0: moveUp(row, col);
            return;
        case 1: moveDown(row, col);
            return;
        case 2: moveRight(row, col);
            return;
        case 3: moveLeft(row, col);
            return;
        default: return; //nemuze v tomto pripade nastat
    }
}
void Animal::moveUp(int row, int col) {
    if (this->row - 1 < 0) {
        move(row, col);
    }
    else {
        this->row--;
    }
}
void Animal::moveDown(int row, int col) {
    if (this->row + 1 >= row) {
        move(row, col);
    }
    else {
        this->row++;
    }
}
void Animal::moveRight(int row, int col) {
    if (this->column + 1 >= col) {
        move(row, col);
    }
    else {
        this->column++;
    }
}
void Animal::moveLeft(int row, int col) {
    if (this->column - 1 < 0) {
        move(row, col);
    }
    else {
        this->column--;
    }
}
void Animal::setThirst(int drank){
   thirst = drank;
}

void Animal::setHunger(int ate){
    hunger = ate;
}

void Animal::eat(Field field) {

}

void Animal::drink(Field field) {
    if(field.isWater()){
        fillThirst();
        std::cout << getSpecies() << " just drank." << std::endl;
    }
    else{
        thirst--;
    }
}

std::string Animal::getSpecies() const {
    return species;
}

int Animal::getSize() const {
    return size;
}

int Animal::getRow() const {
    return row;
}

int Animal::getColumn() const {
    return column;
}

void Animal::fillHunger() {
    hunger = max_hunger;
}

void Animal::fillThirst() {
    thirst = max_thirst;
}

bool Animal::hungerIsSated() {
    return (hunger == max_hunger);
}

Animal::Animal(int maxThirst, int maxHunger, int size, const std::string &species) : max_thirst(maxThirst),
                                                                                     max_hunger(maxHunger), size(size),
                                                                                     species(species) {
    hunger = maxHunger;
    thirst = maxThirst;
}

void Animal::setRow(int row) {
    Animal::row = row;
}

void Animal::setColumn(int column) {
    Animal::column = column;
}
